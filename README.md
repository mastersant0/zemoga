# Zemoga UI candidates test v2.0
[https://github.com/zemoga/ui-test](https://github.com/zemoga/ui-test)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md).

## How to run

Install dependencies
```
yarn install
```

### For development:
```
yarn run watch:dev    // starts the watch server (rebuild on code change)
yarn run start:dev    // starts the dev server (refresh browser on code change)
```

### GraphQL Playground
[GraphQL Playground](http://localhost:8000/graphql)


### To run production server:
```
yarn run build
yarn start
```

## React Themes
[Airbnb react-with-styles](https://airbnb.io/projects/react-with-styles/)



In the project directory, you can run:

### `yarn`

Install dependencies.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## GraphQL endpoint

[https://07v1v7vn75.lp.gql.zone/graphql](https://07v1v7vn75.lp.gql.zone/graphql)

### Launchpad

[https://launchpad.graphql.com/07v1v7vn75](https://launchpad.graphql.com/07v1v7vn75)