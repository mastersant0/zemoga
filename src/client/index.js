import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
// import {SubscriptionClient, addGraphQLSubscriptions} from 'subscriptions-transport-ws';
import { ApolloClient } from 'apollo-client';
import { getMainDefinition } from 'apollo-utilities';
import { ApolloLink, split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { BrowserRouter } from 'react-router-dom';

import { register } from '../serviceWorker';

import getRoutes from 'routes';

require('media/favicon.png');

const config = window.__CONFIG__; // eslint-disable-line no-undef, no-underscore-dangle

window.__APOLLO_STATE__ = null;
window.__CONFIG__ = null;

const httpLink = new HttpLink({
  // uri: config.apiUrl,
  uri: 'http://localhost:8000/graphql',
  // credentials: 'include',
  // credentials: 'same-origin',
});

const wsLink = new WebSocketLink({
  uri: 'ws://localhost:8000/graphql',
  // uri: 'ws://localhost:8000/subscriptions',
  // uri: config.apiUrl,
  options: {
    reconnect: true,
  },
});

const terminatingLink = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return (
      kind === 'OperationDefinition' && operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

const link = ApolloLink.from([terminatingLink]);

const cache = new InMemoryCache().restore(window.__APOLLO_STATE__); // eslint-disable-line no-undef, no-underscore-dangle

const client = new ApolloClient({
  link,
  // link: httpLink,
  cache,
  ssrMode: config.ssrMode,
});

// const renderMethod = !!module.hot ? ReactDOM.render : ReactDOM.hydrate;
// renderMethod(
// ReactDOM.render(
ReactDOM.hydrate(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <div>
        {getRoutes()}
      </div>
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('app')
);

register(config);