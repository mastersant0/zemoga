import authors from './authors';
import posts from './posts';

export default {
  posts,
  authors,
};
