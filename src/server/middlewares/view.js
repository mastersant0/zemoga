import ReactDOM from 'react-dom/server';
import React from 'react';
// import ReactDOM from 'react-dom';
// import { render } from 'react-dom';
import { ApolloClient } from 'apollo-client';
// import ApolloClient from 'apollo-client';
import { StaticRouter } from 'react-router-dom';
import { ApolloProvider, getDataFromTree } from 'react-apollo';
// import { ApolloProvider, renderToStringWithData } from 'react-apollo';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { SchemaLink } from 'apollo-link-schema';

// import { ApolloLink } from 'apollo-link';
// import { withClientState } from 'apollo-link-state';

// import { createHttpLink, HttpLink } from 'apollo-link-http';

// import { ApolloProvider, getDataFromTree } from 'react-apollo';
// import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
// import { makeExecutableSchema } from 'graphql-tools';

import logger from 'utils/logger';
import Html from '../Html';
import config from '../config';
import getRoutes from 'routes';
import schema from '../graphql/schema';
// import defaults from '../graphql/defaults';
// import typeDefs from '../graphql/typeDefs';
// import resolvers from '../graphql/resolvers';

function View(req, res, next) {

  if (process.env.NODE_ENV !== 'production') {
    global.webpackIsomorphicTools.refresh();
  }
  /*
  addMockFunctionsToSchema({
    schema,
    mocks: resolvers,
  });
  */

  const cache = new InMemoryCache();
  // TODO: MOCK schema: https://www.apollographql.com/docs/link/links/schema.html
  const link = new SchemaLink({
    schema,
    context: {},
  });
  // const stateLink = withClientState({ resolvers, cache, defaults });


  const client = new ApolloClient({
    link,
    cache,
    ssrMode: config.get('ssrMode'),
    // ssrMode: true,
    // link: ApolloLink.from([stateLink, new HttpLink()]),
    /*
    link: createHttpLink({
      uri: 'http://localhost:8000',
      credentials: 'same-origin',
      headers: {
        cookie: req.header('Cookie'),
      },
    }),
    */
  });

  const component = (
    <ApolloProvider client={client}>
      <StaticRouter location={req.url} context={{}}>
        <div>
          {getRoutes()}
        </div>
      </StaticRouter>
    </ApolloProvider>
  );

  const configForClient =  {
    apiUrl: config.get('api:url'),
    ssrMode: config.get('ssrMode'),
  };
  /*
  renderToStringWithData(component).then((content) => {
    const initialState = client.extract();
    // const html = <Html content={content} state={initialState} />;
    const htmlComponent = (
      <Html
        content={content}
        state={initialState}
        config={configForClient}
        assets={global.webpackIsomorphicTools.assets()}
      />
    );

    res.status(200);
    res.send(`<!doctype html>\n${ReactDOM.renderToStaticMarkup(htmlComponent)}`);
    res.end();
    */
  getDataFromTree(component).then(() => {
    // const content = ReactDOM.render(component);
    // const content = render(component);
    // const content = ReactDOM.renderToStaticMarkup(component);
    const content = ReactDOM.renderToString(component);
    const initialState = client.extract();
    const htmlComponent = (
      <Html
        content={content}
        state={initialState}
        config={configForClient}
        assets={global.webpackIsomorphicTools.assets()}
      />
    );
    const html = `<!doctype html>\n${ReactDOM.renderToStaticMarkup(htmlComponent)}`;
    res.writeHead(200, {
      'Content-Type': 'text/html',
      'Content-Length': Buffer.byteLength(html),
    });
    res.write(html);
    res.end();
    /*
    res.status(200);
    res.send(`<!doctype html>\n${ReactDOM.renderToStaticMarkup(htmlComponent)}`);
    res.end();
    */
    // const htmlComponent = <Html content={content} state={initialState} />;
    /*
    const htmlComponent = (
      <Html
        content={content}
        state={client.extract()}
        config={configForClient}
        assets={global.webpackIsomorphicTools.assets()}
      />
    );
    res.status(200);
    res.send(`<!doctype html>\n${ReactDOM.renderToStaticMarkup(htmlComponent)}`);
    res.end();
    */

    /*
    const html = `<!doctype html>\n${ReactDOM.renderToStaticMarkup(htmlComponent)}`;
    res.writeHead(200, {
      'Content-Type': 'text/html',
      'Content-Length': Buffer.byteLength(html),
    });
    res.write(html);
    res.end();
    */
  }).catch((err) => {
    // console.log('>>> ERROR', err);
    logger.error('RENDERING ERROR', err);
    next(err);
  });
}


export default View;
