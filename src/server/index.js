import url from 'url';
import Express from 'express';
import { createServer } from 'http';
import { ApolloServer } from 'apollo-server-express';

import config from './config';
import typeDefs from './graphql/typeDefs';
import resolvers from './graphql/resolvers';
import staticFiles from './middlewares/static';
import view from './middlewares/view';

class Server {
  start() {

    const app = Express();

    const server = new ApolloServer({
      typeDefs,
      resolvers,
    });

    server.applyMiddleware({ app });

    app.use('/static', staticFiles());

    app.get('/*', view);

    const httpServer = createServer(app);
    server.installSubscriptionHandlers(httpServer);

    httpServer.listen({port: config.get('server:port')}, () => {
      console.log(`🚀 Server ready at ${url.format(config.get('server'))}`);
      console.log(`🚀 Server ready at http://localhost:${config.get('server:port')}${server.graphqlPath}`);
      console.log(`🚀 Subscriptions ready at ws://localhost:${config.get('server:port')}${server.subscriptionsPath}`)
    });
  }
}

export default Server;
