import React from 'react';
import { themr } from 'react-css-themr';
import { hot } from 'react-hot-loader'

import defaultTheme from './theme.scss';

import Posts from 'components/Posts';

@themr('IndexRoute', defaultTheme)
class IndexRoute extends React.PureComponent {
  render() {
    return (
      <div className={this.props.theme.wrapper}>
        <br />
        <br />
        Default Route!
        <Posts />
      </div>
    );
  }
}

export default hot(module)(IndexRoute);
