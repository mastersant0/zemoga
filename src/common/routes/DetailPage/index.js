// import IndexRoute from './components';
import DetailPage from './components';
// import Home from '../../../components/Home';
// import DetailPage from '../../../components/DetailPage';

export default {
  path: '/another',
  name: 'another',
  component: DetailPage,
  /*
  path: '/',
  exact: true,
  component: IndexRoute,
  */
};


/*
{
  path: '/',
    name: 'home',
  exact: true,
  component: Home,
},
{
  path: '/another',
    name: 'another',
  component: DetailPage,
},
*/
