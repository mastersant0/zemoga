import React from 'react';
import { themr } from 'react-css-themr';
import { hot } from 'react-hot-loader'

import defaultTheme from './theme.scss';

// import type { Page } from '../../../../types';


/*
const DetailPage = (props: Page) => {
  const { title } = props;
*/

@themr('DetailPage', defaultTheme)
class DetailPage extends React.PureComponent {
  render() {
    return (
      <div className={this.props.theme.wrapper}>
        <br />
        <br />
        <article className="pa3 pa5-ns">
          <h1 className="f3 f1-m f-headline-l">Detail Page</h1>
          <p className="measure lh-copy">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
            tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
            vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
            no sea takimata sanctus est Lorem ipsum dolor sit amet.
          </p>
          <p className="measure lh-copy">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
            tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
            vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
            no sea takimata sanctus est Lorem ipsum dolor sit amet.
          </p>
        </article>
      </div>
    );
  }
}

export default hot(module)(DetailPage);

//export default DetailPage;