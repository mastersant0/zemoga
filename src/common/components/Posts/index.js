import PropTypes from 'prop-types';
// import React, { Fragment } from 'react';
import React from 'react';
import classNames from 'classnames';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import {hot} from 'react-hot-loader';

import PostList from './PostList';
// import PostItem from './PostItem';
import { themr } from 'react-css-themr';
import defaultTheme from './theme.scss';


/*
const GET_POSTS = gql`
  query {
    posts @client {
      id
      imageUrl
      title
      author {
        name
      }
      votes {
        dislikes
        likes
      }
    }
  }
`;
*/
const GET_POSTS = gql`
  query {
    posts {
      id
      imageUrl
      title
      author {
        id
        name
      }
      votes {
        dislikes
        likes
      }
    }
  }
`;




@themr('Posts', defaultTheme)
class Posts extends React.PureComponent {

  static propTypes = {
    className: PropTypes.string,
  };

  static defaultProps = {
    className: '',
  };

  render() {
    const {
      theme,
    } = this.props;
    return (
      <Query query={GET_POSTS}>
        {({ data, loading, error, subscribeToMore }) => {
          if (!data) return null;
          if (loading) return <p>Loading...</p>;
          if (error) return <p>Sorry! Something went wrong :(</p>;

          return (
            <section>
              <h2>Votes</h2>
              <PostList
                posts={data.posts}
                subscribeToMore={subscribeToMore}
              />
            </section>
          )
        }}
      </Query>
    );
  }
}

/*
const Posts = () => (
  <Query query={GET_POSTS}>
    {({ data, loading, error, subscribeToMore }) => {
      if (!data) return null;
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Sorry! Something went wrong :(</p>;

      return (
        <section>
          <h2>Votes</h2>
          <PostList
            posts={data.posts}
            subscribeToMore={subscribeToMore}
          />
        </section>
      )
    }}
  </Query>
);
*/
/*
const PostList = () => (
  <Query query={GET_POSTS}>
    {({ loading, error, data }) => {
      // console.log('>>>>>>>> PostList >>>> this >>>>', this);
      // console.log('>>>>>>>> PostList >>>> data >>>>', data);
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;
      return (
        <section>
          <h2>Votes</h2>
          {
            data.posts.map((currentPost) => (
              <PostItem key={currentPost.id} post={currentPost} />
            ))
          }
        </section>
      )
    }}
  </Query>
);
*/

// export default PostList;
export default hot(module)(Posts);