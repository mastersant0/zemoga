// import React, { Fragment } from 'react';
import React from 'react';
// import { Query } from 'react-apollo';
import { Subscription } from 'react-apollo';

import gql from 'graphql-tag';

import {hot} from 'react-hot-loader';

import type { Post } from '../../../types';

import PostItem from './PostItem';
import classNames from 'classnames';
import {themr} from 'react-css-themr';
import defaultTheme from './theme.scss';


type Props = {
  posts: [Post];
  subscribeToMore: any;
  theme: any;
};

const ON_POST_LIKE = gql`
  subscription onPostLike($id: Int!) {
    onPostLike(id: $id) {
      id
      votes {
        likes
      }
    }
  }
`;

@themr('PostList', defaultTheme)
class PostList extends React.Component {

  constructor(props: Props) {
    super(props);
  }

  componentDidMount() {
    this.props.subscribeToMore({
      document: ON_POST_LIKE,
      updateQuery: (prev, {subscriptionData}) => {
        if (!subscriptionData.data) return prev;
        return {
          posts: [
            subscriptionData.data.onPostLike,
            ...prev.posts,
          ],
        };
      },
    });
  }

  render() {
    const { theme } = this.props;
    return (
      <section className={classNames(theme.postList, this.props.className)}>
        {this.props.posts.map(post => (
          <article
            className={classNames(theme.post)}
            key={post.id}
          >
            <PostItem
              post={post}
              theme={theme}
            />
            <Subscription
              subscription={ON_POST_LIKE}
              variables={{ id: post.id }}
            >
              {() => {return null;}}
            </Subscription>
          </article>
        ))}
      </section>
    );
  }
}

// export default PostList;
export default hot(module)(PostList);