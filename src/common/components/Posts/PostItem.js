import React, { Component } from 'react';

import { Mutation } from 'react-apollo';
// import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';


import type { Post } from '../../../types';
import {hot} from 'react-hot-loader';

import classNames from 'classnames';


type Props = {
  post: Post;
  theme: any;
};

const LIKE_MUTATION = gql`
  mutation likePost($id: Int!) {
    likePost(id: $id) {
      id
      votes {
        likes
      }
    }
  }
`;

class PostItem extends Component{

  constructor(props: Props) {
    super(props);
  }

  render(){
    const { post, theme } = this.props;
    // props.subscription
    const {
      id,
      author,
      imageUrl,
      title,
      votes,
    } = post;

    return(
      <div
        //className="fl w-100 w-50-ns pa2 white"
        className={classNames(theme.content)}
      >
        <img
          style={{ 'maxWidth': '100%' }} src={imageUrl} alt={`${author.name} - ${title}`}
        />
        <div className="absolute bottom-0 flex flex-column">
          <h2 className="f2">{author.name}</h2>
          <p>{title}</p>
          <div className="white">
            <Mutation mutation={LIKE_MUTATION} key={id}>
              {LIKE_MUTATION => (
                <button
                  className="ml1 bg-green white f11"
                  onClick={() => {
                    LIKE_MUTATION({ variables: { id }});
                  }}
                  type="submit"
                >
                  Like ({votes && votes.likes})
                </button>
              )}
            </Mutation>
          </div>
          <div className="bg-orange h1 overflow-y-hidden">
            <div
              className="bg-green h1 shadow-1"
              style={{
                width: 100,
                // width: `${Math.floor(votes.likes/(votes.likes + votes.dislikes)*100)}%`,
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

// export default PostItem;
export default hot(module)(PostItem);